﻿namespace Mikhailenko.Файлы_проекта.AbstractFactory;

public class DepartmentMathematics : IDepartmentFactory
{
    public ITeaching CreateSubjectTeaching()
    {
        return new Mathematician();
    }

    public INumberHours SetWorkingHours()
    {
        return new MaxHours();
    }
}