﻿namespace Mikhailenko.Файлы_проекта.AbstractFactory;

public class DepartmentPhilosopher : IDepartmentFactory
{
    public ITeaching CreateSubjectTeaching()
    {
        return new Philosopher();
    }

    public INumberHours SetWorkingHours()
    {
        return new MediumHours();
    }
}