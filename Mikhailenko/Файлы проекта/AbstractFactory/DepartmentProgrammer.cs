﻿namespace Mikhailenko.Файлы_проекта.AbstractFactory;

public class DepartmentProgrammer : IDepartmentFactory
{
    public ITeaching CreateSubjectTeaching()
    {
        return new Programmer();
    }

    public INumberHours SetWorkingHours()
    {
        return new MinimumHours();
    }
}