﻿namespace Mikhailenko.Файлы_проекта.AbstractFactory;

public class Teacher
{
    private ITeaching _teaching;
    private INumberHours _numberHours;
    private string _name;

    public Teacher(string name, IDepartmentFactory factory)
    {
        _name = name;
        _teaching = factory.CreateSubjectTeaching();
        _numberHours = factory.SetWorkingHours();
    }

    public void PrintMyParameters()
    {
        Console.WriteLine("Моя должность " + _name);
        _teaching.Teaching();
        _numberHours.AssignHours();
    }
}