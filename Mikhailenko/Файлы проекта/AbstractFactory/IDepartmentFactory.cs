﻿namespace Mikhailenko.Файлы_проекта.AbstractFactory;

public interface IDepartmentFactory
{
    public abstract ITeaching CreateSubjectTeaching();
    public abstract INumberHours SetWorkingHours();
}