﻿using Mikhailenko.Файлы_проекта.AbstractFactory;

namespace Mikhailenko.Файлы_проекта.Composite;

public class Worker : Component
{
    public Worker(Teacher teacher)
        : base(teacher)
    {
    }
}