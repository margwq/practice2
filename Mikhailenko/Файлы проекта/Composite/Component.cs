﻿using Mikhailenko.Файлы_проекта.AbstractFactory;

namespace Mikhailenko.Файлы_проекта.Composite;

public abstract class Component
{
    protected Teacher _teacher;

    public Component(Teacher teacher)
    {
        _teacher = teacher;
    }

    public virtual void Add(Component component)
    {
    }

    public virtual void Remove(Component component)
    {
    }

    public virtual void Print()
    {
        _teacher.PrintMyParameters();
    }
}