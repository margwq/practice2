﻿using Mikhailenko.Файлы_проекта.AbstractFactory;

namespace Mikhailenko.Файлы_проекта.Composite;

public class Boss : Component
{
    private List<Component> components = new List<Component>();

    public Boss(Teacher teacher)
        : base(teacher)
    {
    }

    public override void Add(Component component)
    {
        components.Add(component);
    }

    public override void Remove(Component component)
    {
        components.Remove(component);
    }

    public override void Print()
    {
        Console.WriteLine("Параметры начальника:");
        _teacher.PrintMyParameters();
        Console.WriteLine("Подчинённые:");
        foreach (var component in components)
        {
            component.Print();
        }
    }
}