﻿using Mikhailenko.Файлы_проекта.AbstractFactory;
using Mikhailenko.Файлы_проекта.Composite;

namespace Mikhailenko.Файлы_проекта;

class Program
{
    static void Main(string[] args)
    {
        // Создаём ректора и строим иерархию
        Teacher teacher1 = new Teacher("Ректор 1", new DepartmentMathematics());
        Teacher teacher2 = new Teacher("Заместитель ректора 1", new DepartmentPhilosopher());
        Teacher teacher3 = new Teacher("Декан 1", new DepartmentProgrammer());        
        Teacher teacher4 = new Teacher("Обычный учитель 1", new DepartmentMathematics());
        Teacher teacher5 = new Teacher("Обычный учитель 2", new DepartmentPhilosopher());
        Teacher teacher6 = new Teacher("Обычный учитель 3", new DepartmentMathematics());
        Teacher teacher7 = new Teacher("Обычный учитель 4", new DepartmentProgrammer());
        Teacher teacher8 = new Teacher("Обычный учитель 5", new DepartmentMathematics());
        
        Component rector = new Boss(teacher1);

        Component viceRector = new Boss(teacher2);
        Component dean = new Boss(teacher3);


        Component commonTeacher1 = new Worker(teacher4);
        Component commonTeacher2 = new Worker(teacher5);
        Component commonTeacher3 = new Worker(teacher6);
        Component commonTeacher4 = new Worker(teacher7);
        Component commonTeacher5 = new Worker(teacher8);

        rector.Add(viceRector);
        rector.Add(commonTeacher1);
        viceRector.Add(commonTeacher5);
        viceRector.Add(dean);
        dean.Add(commonTeacher2);
        dean.Add(commonTeacher3);
        dean.Add(commonTeacher4);

        rector.Print();
        Console.Read();
    }
}